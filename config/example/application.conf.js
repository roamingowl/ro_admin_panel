'use strict';
let configuration = module.exports = {
    listen: {
        port: 3000
    },
    storage: {
        mysql: {
            dbName: 'ro_admin2',
            user: 'root',
            password: 'root',
            host: '127.0.0.1'
        }
    }
}
