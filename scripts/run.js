#!/usr/bin/node
'use strict';
const cluster = require('cluster');
const os = require('os');

cluster.on('exit', function (worker) {

    // Replace the dead worker,
    // we're not sentimental
    console.log('Worker %d died :(', worker.id);
    cluster.fork();

});

if (cluster.isMaster) {
    var cpuCount = os.cpus().length;

    // Create a worker for each CPU
    for (let i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
} else {
    let index = require('./../index.js');
    console.log('Worker %d running!', cluster.worker.id);
}

