'use strict';
const express = require('express');
const EventEmitter = require('events');
const Router = require('./router');

module.exports = class Application extends EventEmitter {
    constructor(configuration) {
        super();
        this.expressApp = null;
        this.configuration = configuration;
    }

    listen() {
        if (this.expressApp) {
            console.log('Application is already listening!')
            return;
        }
        this.expressApp = new express();
        //setup express
        let router = new Router(this.expressApp);
        try {
            router.initRoutes();
        } catch (err) {
            this.emit('error', err);
            return;
        }

        this.expressApp.listen(this.configuration.listen.port, (err) => {
            if (err) {
                this.emit('error', err);
                return;
            }
            this.emit('listen', this.configuration.listen.port);
        })
    }
}