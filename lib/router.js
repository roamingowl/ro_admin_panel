'use strict';
module.exports = class Router {
    constructor(expressApp) {
        this.expressApp = expressApp;
        this.routes = [];
    }

    initRoutes() {
        //TODO: manual routes for now!
        this.addRoute('get', '/', require('./routes/index'), {name: 'index'});
        this.printRoutes();
    }

    addRoute(type, url, routeBody, options) {
        this.routes.push({type: type, url: url.toLowerCase(), body: routeBody, options: options});
        this.expressApp[type.toLowerCase()](url.toLowerCase(), function (req, res, next) {
            routeBody(req, res)
                .then(function () {
                    return next()
                })
                .catch(function (err) {
                    next(err)
                })
        });
    }

    printRoutes() {
        this.routes.forEach((route) => {
            let name = (route.options && route.options.name) ? '@' + route.options.name : '';
            console.log(`${route.type.toUpperCase()}: ${route.url} ${name}`);
        });
    }
}